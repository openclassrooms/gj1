# Horror Game

**Horror Game** est le thème de la première Game Jam de TotemClassrooms.  

## Objectif

Créer un jeu sur le thème *"Horror Game"*. Il est possible de s'orienté vers deux autres thèmes :  

  - Solidarité et entre-aide
  - CyberPunk

### Outils et technologies utilisés

  - **VScode :** Environnement de Développement Intégré (IDE)
  - **JS :** langage de développement
  - **PixiJS :** moteur de création HTML5 basé sur WebGL 2D
  - **Draw.io :** dessin de diagrammes en ligne
  - **Bitbucket :** forge logiciel (dépôt de code source, GugTracker ...)
  - **Git :** gestionnaire de version de code source

### Planification

Dans un premier temps :  

  - trouver une idée de jeu
  - développer le scénario du jeu et son game play
  - se former sur JS et PixiJS

TODO  

Avancement par versions :  

  - **v0.x :** description
  - **v0.y :** description

## Réalisation

### Introduction du jeu

*Tu es un CyberPunk dans un monde apocalyptique, des morts-vivants de partout... Heureusement tu es un Cyborg donc les zombies & CO ne peuvent pas grand chose contre toi car ils ne se nourrissent que d'être vivants. Par contre tu as besoins d'énergie ... Tu vas devoir collaborer avec les êtres vivants pour récupérer/gagner de l'énergie afin de (sur-)vivre.*  

### Description du jeu

#### Personnages

  - cyborg : il n'en reste plus qu'un et c'est toi (seul ton cerveau n'est pas synthétique)
  - humains
  - morts-vivants
  - animaux

#### Terrains

  - cahotique (majoritairement peuplé de morts-vivants)
  - abandonnées a la nature (majoritairement peuplé par la faunes, la flores et les animaux)
  - en reconstruction par les humains (autant peuplé par les êtres vivants que les morts-vivants)

## Conclusion

TODO  

## Sources

[TotemClassRooms 1](https://itch.io/jam/totemclassrooms-1)  
[PixiJS](https://www.pixijs.com/tutorials)  
